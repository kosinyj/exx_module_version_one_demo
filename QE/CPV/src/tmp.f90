subroutine  screen_proto_subdomains_spherical()
  implicit none
  real(dp) :: rcut_tmp(4), rcut_sort(4), large_r
  integer  :: n_tmp(4)
  integer  :: order_rcut(4)
  integer  :: idx_min
  integer  :: idx
  integer  :: np, npsp1, npsp2, npsp3, npsp4
  real(dp) :: dq(3),dqs(3), dist
  integer  :: i,j,k, tmp
  if (is_debug) write(f_debug,*) '<screen_proto_subdomains_spherical>'
  !
  rcut_tmp = (/ rpe(1), rpe(2), rme(1), rme(2) /)
  n_tmp = (/ npe(1), npe(2), nme(1), nme(2) /)
  large_r = maxval(rcut_tmp,1)*100.d0
  do idx = 1, 4
    idx_min           = minloc(rcut_tmp,1)
    rcut_sort(idx)    = rcut_tmp(idx_min)
    rcut_tmp(idx_min) = large_r
    order_rcut(idx)   = idx_min
  end do ! idx
  !
  np = 0; npsp1 = 0; npsp2 = 0; npsp3 = 0; npsp4 = 0
  do k = 1, ngrid(3)
    do j = 1, ngrid(2)
      do i = 1, ngrid(1)
        np = np + 1
        thdtood(i,j,k) = np
        !
        ! distances between Grid points and center of the simulation cell in S space
        ! center of the box is set to grid point at int(ngrid(1)/2), int(ngrid(2)/2), int(ngrid(3)/2) for every cell
        dqs(:) = dble( (/i,j,k/) - c0_grid_idx(:) ) / dble(ngrid(1))
        !
        ! Here we are computing distances between Grid points and center of the simulation cell, so no MIC is needed ...
        ! Compute distance between grid point and the center of the simulation cell in R space 
        dq(1)=h(1,1)*dqs(1)+h(1,2)*dqs(2)+h(1,3)*dqs(3)   !r_i = h s_i
        dq(2)=h(2,1)*dqs(1)+h(2,2)*dqs(2)+h(2,3)*dqs(3)   !r_i = h s_i
        dq(3)=h(3,1)*dqs(1)+h(3,2)*dqs(2)+h(3,3)*dqs(3)   !r_i = h s_i
        !
        dist = dsqrt(dq(1)*dq(1)+dq(2)*dq(2)+dq(3)*dq(3))
        if (dist .le. rcut_sort(1)) then
          npsp1 = npsp1 + 1
          inv_g0(i,j,k) = npsp1
          g0(:,npsp1)   = (/ i, j, k /)
          rbar(:,npsp1) = (/ dq(1), dq(2), dq(3) /)
          sbar(:,npsp1) = (/ dqs(1), dqs(2), dqs(3) /)
        else if (dist .le. rcut_sort(2)) then
          npsp2 = npsp2 + 1
          tmp = npsp2 + n_tmp(order_rcut(1))
          inv_g0(i,j,k) = tmp
          g0(:,tmp)     = (/ i, j, k /)
          rbar(:,tmp)   = (/ dq(1), dq(2), dq(3) /)
          sbar(:,tmp)   = (/ dqs(1), dqs(2), dqs(3) /)
        else if (dist .le. rcut_sort(3)) then
          npsp3 = npsp3 + 1
          tmp = npsp3 + n_tmp(order_rcut(2))
          inv_g0(i,j,k) = tmp
          g0(:,tmp)     = (/ i, j, k /)
          rbar(:,tmp)   = (/ dq(1), dq(2), dq(3) /)
          sbar(:,tmp)   = (/ dqs(1), dqs(2), dqs(3) /)
        else if (dist .le. rcut_sort(4)) then
          npsp4 = npsp4 + 1
          tmp = npsp4 + n_tmp(order_rcut(3))
          inv_g0(i,j,k) = tmp
          g0(:,tmp)     = (/ i, j, k /)
          rbar(:,tmp)   = (/ dq(1), dq(2), dq(3) /)
          sbar(:,tmp)   = (/ dqs(1), dqs(2), dqs(3) /)
        end if
      end do !i
    end do !j
  end do !k
  !
  if (is_debug) then
    !tested
    write(f_debug,*) 'g0'
    write(f_debug,*)  g0
    write(f_debug,*) 'inv_g0'
    write(f_debug,*)  inv_g0
    write(f_debug,*) 'rbar'
    write(f_debug,*)  rbar
    write(f_debug,*) 'sbar'
    write(f_debug,*)  sbar
    write(f_debug,*) '</screen_proto_subdomains_spherical>'
  end if
  return
end subroutine screen_proto_subdomains_spherical
