#!/bin/bash
nw=128
iter=50

for i in `ls -d ./MPI0512.gga/ ./MPI0512.gga.no-hyperthreads/`
do
  cd $i

  nprc=`grep move_electro fort.1????|wc -l`
  zeta=`echo "$nw $nprc"| awk '{print $2/($1*4)}'`

  rm -f timings.* mlwf_summary.dat

  # pbe
  grep '\<move_electro\>' fort.1???? | awk -v np=$nprc -v it=$iter '{s+=$7}END{print s/np/it}' > timings.pbe

  # MLWF
  grep hk_mlwf      fort.1???? | awk '{s+=$7}END{print "hk_mlwf ",     s}' >> MLWF.timings.sum_all_wall
  awk -v nprc=$nprc -v iter=$iter '{print $1, $2/nprc/iter," s"}' MLWF.timings.sum_all_wall > MLWF.timings.avg
  grep 'MLWF step' output.vc | awk '{s+=$3}END{print s/NR}' > MLWF.avg_steps
  paste MLWF.timings.avg MLWF.avg_steps  > mlwf_summary.dat
  rm -f MLWF.*

  cd ..
done
