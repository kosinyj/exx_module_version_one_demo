#!/bin/bash

rm -f gamma_stat.dat

octave util_stat_gamma.m | grep -v ans|grep -v '^$' | awk -v d=$i '{print d,$0}' >> gamma_stat.dat
