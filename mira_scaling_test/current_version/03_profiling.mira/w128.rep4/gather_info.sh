#!/bin/bash
nw=128
iter=50

for i in `ls -d ./MPI????/`
do
  cd $i

  nprc=`grep exx_01 fort.1????|wc -l`
  zeta=`echo "$nw $nprc"| awk '{print $2/($1*4)}'`

  rm -f timings.* mlwf_summary.dat
  # get timings
  grep exx_01_comm  fort.1???? | awk '{s+=$7}END{print "exx_01_comm ", s}' >  timings.sum_all_wall
  grep exx_02_comp  fort.1???? | awk '{s+=$7}END{print "exx_02_comp ", s}' >> timings.sum_all_wall
  grep exx_03_comm  fort.1???? | awk '{s+=$7}END{print "exx_03_comm ", s}' >> timings.sum_all_wall
  grep exx_03_comp  fort.1???? | awk '{s+=$7}END{print "exx_03_comp ", s}' >> timings.sum_all_wall
  grep exx_03_idle  fort.1???? | awk '{s+=$7}END{print "exx_03_idle ", s}' >> timings.sum_all_wall
  grep exx_04_pcomp fort.1???? | awk '{s+=$7}END{print "exx_04_pcomp", s}' >> timings.sum_all_wall
  grep exx_04_scomp fort.1???? | awk '{s+=$7}END{print "exx_04_scomp", s}' >> timings.sum_all_wall
  grep exx_04_me    fort.1???? | awk '{s+=$7}END{print "exx_04_me   ", s}' >> timings.sum_all_wall
  grep exx_05_comm  fort.1???? | awk '{s+=$7}END{print "exx_05_comm ", s}' >> timings.sum_all_wall
  grep exx_05_comp  fort.1???? | awk '{s+=$7}END{print "exx_05_comp ", s}' >> timings.sum_all_wall
  grep exx_05_idle  fort.1???? | awk '{s+=$7}END{print "exx_05_idle ", s}' >> timings.sum_all_wall
  grep exx_06_comm  fort.1???? | awk '{s+=$7}END{print "exx_06_comm ", s}' >> timings.sum_all_wall

  awk -v nprc=$nprc -v iter=$iter '{print $1, $2/nprc/iter," s"}' timings.sum_all_wall | column -t > timings.avg
  rm -f timings.sum_all_wall

  # creative accounting
  echo "#comp[s] fcomp[%] comm[s] fcomm[%] idle[s] fidle[%] total[s]" > timings.avg.crt_acc
  awk '(NR==1){comm+=$2}
       (NR==2){comp+=$2}
       (NR==3){comm+=2*$2}
       (NR==4){comp+=$2}
       (NR==6){comp+=$2}
       (NR==7){comp+=$2}
       (NR==10){comp+=$2}
       (NR==12){comm+=$2}
       (NR!=8){tot+=$2}
       END{{idle=tot-comp-comm}
           {print comp, comp/tot*100, comm, comm/tot*100, idle, 100-comp/tot*100-comm/tot*100, tot}}' timings.avg >> timings.avg.crt_acc
  column -t timings.avg.crt_acc > timings.avg.crt_acc_
  mv timings.avg.crt_acc_ timings.avg.crt_acc

  # pbe
  grep '\<move_electro\>' fort.1???? | awk -v np=$nprc -v it=$iter '{s+=$7}END{print s/np/it}' > timings.mv_e
  grep '\<exact_exchan\>' fort.1???? | awk -v np=$nprc -v it=$iter '{s+=$7}END{print s/np/it}' > timings.exx_with_unnecessary_fft
  pbe=`paste timings.mv_e timings.exx_with_unnecessary_fft | awk '{print $1-$2}'`
  rm -f timings.exx_with_unnecessary_fft  timings.mv_e
  echo "PBE time / CPMD step [s] : $pbe" > timings.pbe


  # MLWF
  grep hk_mlwf      fort.1???? | awk '{s+=$7}END{print "hk_mlwf ",     s}' >> MLWF.timings.sum_all_wall
  awk -v nprc=$nprc -v iter=$iter '{print $1, $2/nprc/iter," s"}' MLWF.timings.sum_all_wall > MLWF.timings.avg
  grep 'MLWF step' output.vc | awk '{s+=$3}END{print s/NR}' > MLWF.avg_steps
  paste MLWF.timings.avg MLWF.avg_steps  > mlwf_summary.dat
  rm -f MLWF.*

  cd ..
done
