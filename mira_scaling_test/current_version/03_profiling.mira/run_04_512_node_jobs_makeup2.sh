#!/bin/bash

# JOB INFO
# parent block : 512 nodes 

# path for cp.x executable...
CP=/gpfs/mira-fs1/projects/CrystalsADSP/hsinyu/exx_module_version_one_demo/QE/CPV/src/cp.x_profile_hdf5.exx_03_barrier.no_io

pushd ./w128.rep4/MPI0512/
runjob -n 512 -p 1 --block $COBALT_PARTNAME --envs OMP_NUM_THREADS=64 --verbose=INFO : $CP -ntg 1 -ndiag 512  < ./input.vc.1s > output.vc 
popd

pushd ./w128.rep5/MPI0512/
runjob -n 512 -p 1 --block $COBALT_PARTNAME --envs OMP_NUM_THREADS=64 --verbose=INFO : $CP -ntg 1 -ndiag 512  < ./input.vc.1s > output.vc 
popd
