#!/bin/bash

###################################################################
# SUBMISSION SCRIPT FOR HYBRID ENSEMBLE AND SUBBLOCK JOBS ON BG/Q #
###################################################################

# JOB INFO
# parent block : 512 nodes 
# sub blocks   : 1x512 nodes 
# jobs         : 4 x 256 nodes

echo "STARTING HYBRID ENSEMBLE-SUBBLOCK SCRIPT" `date`

echo "COBALT_PARTNAME:" $COBALT_PARTNAME 

# path for cp.x executable...
CP=/gpfs/mira-fs1/projects/CrystalsADSP/hsinyu/exx_module_version_one_demo/QE/CPV/src/cp.x_profile_hdf5.exx_03_barrier.no_io

# Get all available bootable blocks of given size within parent block (specified by cobalt job submission script)...
BLOCKS1=`get-bootable-blocks --size 512 $COBALT_PARTNAME`
echo "BLOCKS FOUND: " 
echo $BLOCKS1 

# Boot all available blocks one at a time...
b=0 #block counter...
for BLOCK in $BLOCKS1
do
  ((b++))
  if [ $b -le 2 ]; then
    boot-block --block $BLOCK & 
  fi
done
wait

# Submit jobs in a given block...
b=0 #block counter...
for BLOCK in $BLOCKS1
do

  # Increment block/job counter...
  ((b++))

  if [ $b -eq 1 ]; then

    echo "RUNNING ON BLOCK: " $BLOCK

    # Get corners and general location arguments for a given block... 
    LOCARGS="--block $BLOCK ${COBALT_CORNER:+--corner} $COBALT_CORNER ${COBALT_SHAPE:+--shape} $COBALT_SHAPE"
    ## 256 nodes (cetus)
    SHAPE=4x2x4x4x2
    CORNERS=$(/soft/cobalt/bgq_hardware_mapper/get-corners.py $BLOCK $SHAPE)
    echo "  CORNERS FOUND: " $CORNERS

    # Submit jobs in each corner (jobs in different directories)...
    j=0 #job counter...
    for CORNER in $CORNERS
    do
      # Increment job counter...
      ((j++))

      if [ $j -eq 1 ]; then
        #
        echo "RUNNING ON CORNER: " $CORNER
        echo "SUBMITTING JOB JOB01/" `date`

        pushd ./w064.rep4/MPI0256/
        runjob -n 256 -p 1 --block $BLOCK --corner $CORNER --shape $SHAPE --envs OMP_NUM_THREADS=64 --verbose=INFO : $CP  -ndiag 256  < ./input.vc.1s > output.vc &
        popd

        #
      elif [ $j -eq 2 ]; then
        #
        echo "RUNNING ON CORNER: " $CORNER
        echo "SUBMITTING JOB JOB02/" `date`

        sleep 2m

        pushd ./w064.rep5/MPI0256/
        runjob -n 256 -p 1 --block $BLOCK --corner $CORNER --shape $SHAPE --envs OMP_NUM_THREADS=64 --verbose=INFO : $CP  -ndiag 256  < ./input.vc.1s > output.vc
        popd

        #
      fi # $j
    done # $j
    wait


  fi # $b
done # $b
wait


# Free all available blocks one at a time...
for BLOCK in $BLOCKS1
do
  boot-block --block $BLOCK --free &
done
wait

echo "LEAVING HYBRID ENSEMBLE-SUBBLOCK SCRIPT" `date`
