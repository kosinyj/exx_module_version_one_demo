#!/bin/bash

# JOB INFO
# parent block : 512 nodes 

# path for cp.x executable...
CP=/gpfs/mira-fs1/projects/CrystalsADSP/hsinyu/exx_module_version_one_demo/QE/CPV/src/cp.x_profile_hdf5.exx_03_barrier.no_io

pushd ./w128.rep3/MPI0512.gga-only/
runjob -n 512 -p 1 --block $COBALT_PARTNAME --envs OMP_NUM_THREADS=64 --verbose=INFO : $CP -ntg 2 -ndiag 484 < ./input.vc.1s > output.vc 
popd
